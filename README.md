# .dotfiles

This repo contains most of my personal dotfiles.

Dotfiles are managed with GNU Stow. For help, visit the following link: https://alexpearce.me/2016/02/managing-dotfiles-with-stow/

``` bash
stow -vS $prog-dir
```

## Screenshots

![screenshot1](https://raw.githubusercontent.com/dominicbraam/.dotfiles/main/screenshots/2022-06/2022-06-10-135245_2557x1437_scrot.png)
![screenshot2](https://raw.githubusercontent.com/dominicbraam/.dotfiles/main/screenshots/2022-06/2022-06-10-135918_2559x1439_scrot.png)
